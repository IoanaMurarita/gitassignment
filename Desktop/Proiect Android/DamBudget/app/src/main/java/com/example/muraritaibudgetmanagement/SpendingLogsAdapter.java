import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.muraritaibudgetmanagement.IncomeLog;
import com.example.muraritaibudgetmanagement.R;
import com.example.muraritaibudgetmanagement.SpendingLog;

import java.text.SimpleDateFormat;
import java.util.List;

public class SpendingLogsAdapter extends ArrayAdapter {

    private int resource;
    private List<SpendingLog> spendingLogs;
    private LayoutInflater layoutInflater;

    public SpendingLogsAdapter(Context context,
                             int resource,
                             List<SpendingLog> spendingLogs,
                             LayoutInflater layoutInflater) {
        super(context, resource, spendingLogs);
        this.resource = resource;
        this.spendingLogs = spendingLogs;
        this.layoutInflater = layoutInflater;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View view = layoutInflater.inflate(resource, parent, false);
        SpendingLog spendingLog = spendingLogs.get(position);
        if (spendingLog != null) {
            TextView txtValue=view.findViewById(R.id.txt_value4);
            TextView txtDate=view.findViewById(R.id.txt_date4);
            TextView txtPersonId=view.findViewById(R.id.txt_personId4);

            txtValue.setText(String.valueOf(spendingLog.getValue()));
            SimpleDateFormat sdf=new SimpleDateFormat("dd-MM-yyyy");
            txtDate.setText(sdf.format(spendingLog.getDate()));
            txtPersonId.setText(String.valueOf(spendingLog.getId()));
        }
        return view;
    }


}
